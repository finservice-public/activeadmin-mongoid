require 'active_admin/filters/active_filter'
module ActiveAdmin
  module Filters

    class ActiveFilter
      def related_primary_key
        if predicate_association
          predicate_association.primary_key
        elsif related_class
          related_class.primary_key
        end
      end
    end
  end
end
